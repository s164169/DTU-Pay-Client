#!/bin/bash
set -e
mvn clean install -DskipTests
# Build the docker image using
docker build --tag dtupay-client .

# Do a garbage collection of images not used anymore.
docker image prune -f

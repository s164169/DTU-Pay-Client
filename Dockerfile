FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/dtupay_client.jar /usr/src/target/
CMD java -ea -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true -jar target/dtupay_client

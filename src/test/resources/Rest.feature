## new feature
## Tags: optional
#
Feature: REST API
  Background:
    Given They both have a bank account
    And A customer is registered with DTUPay
    And A merchant is registered with DTUPay

  Scenario: A Customer registers with DTUPay
    When A customer signs up
    Then The customer is signed up with DTUPay

  Scenario: A merchant registers with DTUPay
    When A merchant signs up
    Then The merchant is signed up with DTUPay

  Scenario: A customer requests tokens
    Given The customer has 1 token
    When The customer requests 4 tokens
    Then The customer receives an OK response

  Scenario: A customer requests tokens with more than 1 token
    Given The customer has 2 token
    When The customer requests 4 tokens
    Then The customer receives an Bad Request response

  Scenario: A customer transfer money to a merchant
    Given The customer has 1 token
    When A merchant requests payment from a customer
    Then The merchant receives an OK response

  Scenario: A customer requests the report history
    Given The customer has performed a payment with the merchant
    When The customer requests report history
    Then The customer receives an OK response

  Scenario: A merchant requests the report history
    Given The customer has performed a payment with the merchant
    When The merchant requests report history
    Then The merchant receives an OK response

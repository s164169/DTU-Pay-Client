package com.dtupay.client;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
//@author Mathias Boss Jørgensen
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources",
        plugin = { "html:target/cucumber/wikipedia.html"},
        monochrome=true,
        snippets = SnippetType.CAMELCASE,
        glue = {"com.dtupay.client"})
public class AcceptanceTest {

}

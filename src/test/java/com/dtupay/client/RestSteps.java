package com.dtupay.client;

import com.dtupay.client.Models.*;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.*;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
//@author William ben embarek
public class RestSteps {

    public String result, merchantResult, customerResult;
    WebTarget baseUrl, merchantUrl;
    public String customerID, merchantID;
    public UserDTO customer, merchant, getCustomerDTO;
    public Response customerHasTokensResponse, tokenRequestResponse, transferMoneyResponse;
    public int signUpStatus, signUpStatusTokenRequest, scanStatus;
    public MerchantIDDTO merchantIDDTO, registerMerchantResult;
    public CustomerIDDTO customerIDDTO, registerCustomerResult;
    public ScanResultDTO scanResultDTO;
    public TokenDTO customerToken;
    public boolean scanTokenSuccess;
    public String merchantBankAccountID, customerBankAccountID;
    private User merchantUser, customerUser;
    public TokenListDTO tokenListDTO;
    private int statusTokenRequest, statusTransferRequest;
    private RequestTokenDTO hasTokenDTO;
    public TransferMoneyDTO transferMoneyDTO;
    public Invocation customerReportInvocation;
    public RequestTokenDTO requestTokenDTO;
    public PaymentDTO paymentDTO;

    //public IBank bank;
    BankService bank = new BankServiceService().getBankServicePort();
    List<String> bankAccounts = new ArrayList<>();
    private Response customerReportResponse;
    private Invocation merchantReportInvocation;
    private Response merchantReportResponse;


    public RestSteps() throws Exception {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/api/dtupay");
        //baseUrl = client.target("http://fastmoney-01.compute.dtu.dk:8080/api/dtuPay/test");


    }
    //@author Sebastian Fisher
    @After
    public void cleanupUsedAccounts() throws BankServiceException_Exception {
        for (String account : bankAccounts){
            bank.retireAccount(account);
            System.out.println("Retired an account ma homey");
        }
        //System.out.println("Customer account: " + customer.getAccountID());
        //System.out.println("Merchant account: " + merchant.getAccountID());
        //System.out.println(baseUrl.path("testCustomer").request().post(Entity.entity(customer,MediaType.APPLICATION_JSON_TYPE)).getStatus());
        //System.out.println(baseUrl.path("testMerchant").request().post(Entity.entity(merchant, MediaType.APPLICATION_JSON_TYPE)).getStatus());
    }
    //@author Mathias Bo Jensen
    @Given("^They both have a bank account$")
    public void theyBothHaveABankAccount() throws BankServiceException_Exception {
        System.out.println("Both have bank accounts");
        this.merchantUser = new User();
        //merchantUser.setCprNumber("859237-78419");
        merchantUser.setCprNumber(UUID.randomUUID().toString());
        merchantUser.setFirstName("Finn");
        merchantUser.setLastName("Ish");
        merchantBankAccountID = bank.createAccountWithBalance(merchantUser, new BigDecimal(100));
        bankAccounts.add(merchantBankAccountID);

        this.customerUser = new User();
        //customerUser.setCprNumber("230297-48119");
        customerUser.setCprNumber(UUID.randomUUID().toString());
        customerUser.setFirstName("Sebast");
        customerUser.setLastName("Ian");
        customerBankAccountID = bank.createAccountWithBalance(customerUser, new BigDecimal(10000));
        bankAccounts.add(customerBankAccountID);

    }
    //@author Magnus glasdam jakobsen
    @And("^A merchant is registered with DTUPay$")
    public void aMerchantIsRegisteredWithDTUPay() {

        System.out.println("VI ER I Register Merchant TEST NU!!!");
        this.merchant = new UserDTO();
        this.merchantIDDTO = new MerchantIDDTO();
        merchant.setCprNumber(merchantUser.getCprNumber());
        merchant.setFirstName(merchantUser.getFirstName());
        merchant.setLastName(merchantUser.getLastName());
        merchant.setAccountID(merchantBankAccountID);
        System.out.println("TJEST");
        registerMerchantResult = baseUrl.path("/merchants").request().post(Entity.entity(merchant, MediaType.APPLICATION_JSON_TYPE), MerchantIDDTO.class);
        System.out.println("RESULT: " + registerMerchantResult.merchantID);
        this.merchantIDDTO.setMerchantID(registerMerchantResult.merchantID);
        try {
            //merchantIDDTO = baseUrl.path("/merchants/"+merchantResult).request().get(MerchantIDDTO.class);
            Invocation invocation = baseUrl.path("/merchants/"+merchantIDDTO.merchantID).request().buildGet();
            Response response = invocation.invoke();
            int status = response.getStatus();
            System.out.println(status);
            assertEquals(200,status);

        } catch (NotFoundException e) {
            //assertEquals(404, e.getResponse().getStatus());
            System.out.println(e.getResponse().getStatus());
        }
        System.out.println("RESPONSE MERCHANT TEST " + merchantIDDTO.merchantID);

    }
    //@author Frederik Kirkegaard
    @And("^A customer is registered with DTUPay$")
    public void aCustomerIsRegisteredWithDTUPay() {
        System.out.println("VI ER I Register Customer TEST NU!!!");
        this.customer = new UserDTO();
        this.customerIDDTO = new CustomerIDDTO();
        customer.setCprNumber(customerUser.getCprNumber());
        customer.setFirstName(customerUser.getFirstName());
        customer.setLastName(customerUser.getLastName());
        customer.setAccountID(customerBankAccountID);
        System.out.println("CUSTOMER REGISTRATION2");
        registerCustomerResult = baseUrl.path("/customers").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), CustomerIDDTO.class);
        System.out.println("RESULT: " + registerCustomerResult.customerID);
        this.customerIDDTO.setCustomerID(registerCustomerResult.customerID);

        try {
            //customerIDDTO = baseUrl.path("/customers/"+customerIDDTO.customerID).request().get(CustomerIDDTO.class);
            Invocation invocation = baseUrl.path("/customers/"+customerIDDTO.customerID).request().buildGet();
            Response response = invocation.invoke();
            int status = response.getStatus();
            System.out.println(status);
            assertEquals(200,status);

        } catch (NotFoundException e) {
            //assertEquals(404, e.getResponse().getStatus());
            System.out.println(e.getResponse().getStatus());
        }
        System.out.println("RESPONSE Customer: " + customerIDDTO.customerID);
    }
//@author Mathias Boss Jørgensen

    //TODO: Still needs implementation of the bank stuff...
    @When("^A customer signs up$")
    public void aCustomerSignsUp() {

        UserDTO customerSignUp = new UserDTO();
        int signUpStatus;

        customerSignUp.setCprNumber(UUID.randomUUID().toString());
        customerSignUp.setFirstName("Jens");
        customerSignUp.setLastName("Jensen");

        //customerID = baseUrl.path("/registerCustomer").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), String.class);
        Invocation invocation = baseUrl.path("/registerCustomer").request().buildPost(Entity.entity(customerSignUp, MediaType.APPLICATION_JSON_TYPE));
        Response response = invocation.invoke();
        signUpStatus = response.getStatus();
        customerID = response.readEntity(String.class);
        //assertEquals(200,signUpStatus);


    }
    //@author William ben embarek
    @Then("^The customer is signed up with DTUPay$")
    public void theCustomerIsSignedUpWithDTUPay() {
        UserDTO response;
        System.out.println("customerID: "+customerIDDTO.customerID);
        try {
            response = baseUrl.path("/customers/"+customerIDDTO.customerID).request().get(UserDTO.class);
            assertEquals(customerBankAccountID,response.getAccountID());
        } catch (NotFoundException e) {
            assertEquals(404, e.getResponse().getStatus());
            System.out.println(e.getResponse().getStatus());
        }


        //throw new PendingException();
    }
    //@author Sebastian Fisher

    //TODO: Still needs implementation of the bank stuff...
    @When("^A merchant signs up$")
    public void aMerchantSignsUp() {
        UserDTO merchantSignUp = new UserDTO();

        merchantSignUp.setCprNumber(UUID.randomUUID().toString());
        merchantSignUp.setFirstName("Jensemand");
        merchantSignUp.setLastName("Jensenmand");

        //customerID = baseUrl.path("/registerCustomer").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), String.class);
        Invocation invocation = baseUrl.path("/merchants").request().buildPost(Entity.entity(merchant, MediaType.APPLICATION_JSON_TYPE));
        Response response = invocation.invoke();
        signUpStatus = response.getStatus();
        merchantID = response.readEntity(String.class);
        assertEquals(200,signUpStatus);

        //throw new PendingException();
    }

    //@author Mathias Bo Jensen

    @Then("^The merchant is signed up with DTUPay$")
    public void theMerchantIsSignedUpWithDTUPay() {

        try {
            UserDTO response;
            System.out.println("MerchantID: "+merchantIDDTO.merchantID);
            response = baseUrl.path("/merchants/"+merchantIDDTO.merchantID).request().get(UserDTO.class);
            assertEquals(merchantBankAccountID,response.getAccountID());
        } catch (NotFoundException e) {
            assertEquals(404, e.getResponse().getStatus());
            System.out.println(e.getResponse().getStatus());
        }
        //throw new PendingException();
    }

    //@author Magnus glasdam jakobsen
    @Given("^The customer has (\\d+) token$")
    public void theCustomerHasToken(int numberOfTokensRequested) {
        this.hasTokenDTO = new RequestTokenDTO();

        //String customerUUID = baseUrl.path("/registerCustomer").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), String.class);
        hasTokenDTO.setNumberOfTokens(numberOfTokensRequested);
        hasTokenDTO.setCustomerID(customerIDDTO.customerID.toString());

        tokenListDTO = baseUrl.path("/customers/"+customerIDDTO.customerID.toString()+"/tokens").request().post(Entity.entity(hasTokenDTO, MediaType.APPLICATION_JSON_TYPE),TokenListDTO.class);
        //tokenListDTO = baseUrl

/*
        Invocation customerHasTokensInvocation = baseUrl.path("/customer/"+customerIDDTO.customerID+"/tokens").request().buildPost(Entity.entity(hasTokenDTO, MediaType.APPLICATION_JSON_TYPE));
        customerHasTokensResponse = customerHasTokensInvocation.invoke();
        System.out.println("customerHasTokenInvocation: "+customerHasTokensInvocation);
        tokenListDTO = customerHasTokensResponse.readEntity(TokenListDTO.class);*/

        assertEquals(numberOfTokensRequested,tokenListDTO.getTokens().size());

        //throw new PendingException();
    }

    //@author Frederik Kirkegaard
    @When("^The customer requests (\\d+) tokens$")
    public void theCustomerRequestsTokens(int numberOfTokensRequested) {
        this.requestTokenDTO = new RequestTokenDTO();

        //String customerUUID = baseUrl.path("/registerCustomer").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), String.class);
        requestTokenDTO.setNumberOfTokens(numberOfTokensRequested);
        requestTokenDTO.setCustomerID(customerID);
        //tokenListDTO = baseUrl.path("/customers/"+customerIDDTO.customerID.toString()+"/tokens").request().post(Entity.entity(requestTokenDTO, MediaType.APPLICATION_JSON_TYPE),TokenListDTO.class);


        Invocation tokenRequestInvocation = baseUrl.path("/customers/"+customerIDDTO.customerID+"/tokens").request().buildPost(Entity.entity(requestTokenDTO, MediaType.APPLICATION_JSON_TYPE));
        tokenRequestResponse = tokenRequestInvocation.invoke();
        statusTokenRequest = tokenRequestResponse.getStatus();
        //tokenListDTO = tokenRequestResponse.readEntity(TokenListDTO.class);
        //tokenListDTO = baseUrl.path("/customer/"+customerID+"/tokens").request().post(Entity.entity(requestTokenDTO,MediaType.APPLICATION_JSON_TYPE),TokenListDTO.class);
        //tokenListDTO = tokenRequestResponse.readEntity(TokenListDTO.class);
        //assertEquals(numberOfTokensRequested,tokenListDTO.getTokens().size());

        //throw new PendingException();
    }

    //@author Mathias Boss Jørgensen
    @Then("^The customer receives an OK response$")
    public void theCustomerReceivesAnOKResponse() {
        System.out.println("statusTokenRequest: customer receives ok: statusTokenRequest"+statusTokenRequest);
        assertEquals(200,statusTokenRequest);
        //throw new PendingException();

    }
    //@author William ben embarek
    @Then("^The customer receives an Bad Request response$")
    public void theCustomerReceivesAnBadRequestResponse() {
        assertEquals(400,statusTokenRequest);
    }
    /*
    @Then("^The customer has (\\d+) tokens$")
    public void theCustomerHasTokens(int numberOfTokensRequested) {
        RequestTokenDTO requestTokenDTO = new RequestTokenDTO();
        requestTokenDTO.setNumberOfTokens(numberOfTokensRequested);
        requestTokenDTO.setCustomerID(customerID);
        baseUrl.path("/requestTokens").request().post(Entity.entity(requestTokenDTO,MediaType.APPLICATION_JSON_TYPE));

        NumberOfUserTokensDTO numberOfUserTokensDTO = baseUrl.path("/getUsersNumberOfTokens").request().post(Entity.entity(customerUUID,MediaType.APPLICATION_JSON_TYPE), .class);
        assertEquals(numberOfTokensRequested,numberOfUserTokensDTO);
        throw new PendingException();
    }
*/
//@author Sebastian Fisher
    @Then("^yes$")
    public void yes() {
        assertTrue(1 == 1);
    }

    //@author Mathias Bo Jensen

    @When("^A merchant requests payment from a customer$")
    public void aMerchantRequestsPaymentFromACustomer() {
        this.requestTokenDTO = new RequestTokenDTO();

        //String customerUUID = baseUrl.path("/registerCustomer").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), String.class);
        requestTokenDTO.setNumberOfTokens(1);
        requestTokenDTO.setCustomerID(customerIDDTO.customerID.toString());
        //tokenListDTO = baseUrl.path("/customers/"+customerIDDTO.customerID.toString()+"/tokens").request().post(Entity.entity(requestTokenDTO, MediaType.APPLICATION_JSON_TYPE),TokenListDTO.class);

        System.out.println("Print1");
        Invocation tokenRequestInvocation = baseUrl.path("/customers/"+customerIDDTO.customerID+"/tokens").request().buildPost(Entity.entity(requestTokenDTO, MediaType.APPLICATION_JSON_TYPE));
        tokenRequestResponse = tokenRequestInvocation.invoke();
        statusTokenRequest = tokenRequestResponse.getStatus();
        tokenListDTO = tokenRequestResponse.readEntity(TokenListDTO.class);
        System.out.println("Print2");
        paymentDTO = new PaymentDTO();
        paymentDTO.setAmount(new BigDecimal(2));
        paymentDTO.setDescription("rest test");
        paymentDTO.setMerchanID(merchantIDDTO.merchantID);
        paymentDTO.setTokenID(tokenListDTO.getTokens().get(0).getTokenID());
        System.out.println("Print3");
        Invocation transferMoneyInvocation =  baseUrl.path("/merchants/transfer").request().buildPost(Entity.entity(paymentDTO, MediaType.APPLICATION_JSON_TYPE));
        System.out.println("Print4");
        transferMoneyResponse = transferMoneyInvocation.invoke();
        System.out.println("Print5");
        statusTransferRequest = transferMoneyResponse.getStatus();
        System.out.println("Print6");
        assertEquals(200,statusTransferRequest);

    }
    //@author Magnus glasdam jakobsen
    @Then("^The merchant receives an OK response$")
    public void theMerchantReceivesAnOKResponse() {
        assertEquals(200,statusTransferRequest);
    }
    //@author Frederik Kirkegaard
    @Given("^The customer has performed a payment with the merchant$")
    public void theCustomerHasPerformedAPaymentWithTheMerchant() {
        this.requestTokenDTO = new RequestTokenDTO();

        //String customerUUID = baseUrl.path("/registerCustomer").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), String.class);
        requestTokenDTO.setNumberOfTokens(1);
        requestTokenDTO.setNumberOfTokens(1);
        requestTokenDTO.setCustomerID(customerIDDTO.customerID.toString());
        //tokenListDTO = baseUrl.path("/customers/"+customerIDDTO.customerID.toString()+"/tokens").request().post(Entity.entity(requestTokenDTO, MediaType.APPLICATION_JSON_TYPE),TokenListDTO.class);


        Invocation tokenRequestInvocation = baseUrl.path("/customers/"+customerIDDTO.customerID+"/tokens").request().buildPost(Entity.entity(requestTokenDTO, MediaType.APPLICATION_JSON_TYPE));
        tokenRequestResponse = tokenRequestInvocation.invoke();
        statusTokenRequest = tokenRequestResponse.getStatus();
        tokenListDTO = tokenRequestResponse.readEntity(TokenListDTO.class);

        paymentDTO = new PaymentDTO();
        paymentDTO.setAmount(new BigDecimal(2));
        paymentDTO.setDescription("rest test");
        paymentDTO.setMerchanID(merchantIDDTO.merchantID);
        paymentDTO.setTokenID(tokenListDTO.getTokens().get(0).getTokenID());

        Invocation transferMoneyInvocation =  baseUrl.path("/merchants/transfer").request().buildPost(Entity.entity(paymentDTO, MediaType.APPLICATION_JSON_TYPE));
        transferMoneyResponse = transferMoneyInvocation.invoke();

    }
    //@author Mathias Boss Jørgensen
    @When("^The customer requests report history$")
    public void theCustomerRequestsReportHistory() {

        //Invocation transferMoneyInvocation =  baseUrl.path("/merchants/reports").request().buildPost(Entity.entity(, MediaType.APPLICATION_JSON_TYPE));


        //transferMoneyResponse = transferMoneyInvocation.invoke();
        //statusTransferRequest = transferMoneyResponse.getStatus();

        customerReportInvocation = baseUrl.path("/customers/report").queryParam("id",customerIDDTO.customerID.toString()).queryParam("startDate", "10/01/2020").queryParam("endDate","30/01/2020").request().buildGet();
        customerReportResponse = customerReportInvocation.invoke();
        statusTransferRequest = customerReportResponse.getStatus();
        assertEquals(200,statusTransferRequest);



    }
    //@author William ben embarek
    @When("^The merchant requests report history$")
    public void theMerchantRequestsReportHistory() {
        /*transferMoneyDTO = new TransferMoneyDTO();
        transferMoneyDTO.setAmount(new BigDecimal(2));
        transferMoneyDTO.setDescription("Test Description");
        transferMoneyDTO.setFromAccount(customerIDDTO.customerAccountID);
        transferMoneyDTO.setToAccount(merchantIDDTO.merchantAccountID);
        transferMoneyDTO.setTokenID(tokenListDTO.getTokens().get(0).getTokenID());
        //transferMoneyDTO.setTokenStatus(TokenDTO.TokenStatus.values()[0]);

        Invocation transferMoneyInvocation =  baseUrl.path("/merchants/transfer").request().buildPost(Entity.entity(transferMoneyDTO, MediaType.APPLICATION_JSON_TYPE));
        System.out.println("Transfer done");
        transferMoneyResponse = transferMoneyInvocation.invoke();
        statusTransferRequest = transferMoneyResponse.getStatus();*/
        System.out.println("Getting report");
        merchantReportInvocation = baseUrl.path("/merchants/report").queryParam("id",merchantIDDTO.merchantID).queryParam("startDate", "10/01/2020").queryParam("endDate","30/01/2020").request().buildGet();
        merchantReportResponse = merchantReportInvocation.invoke();
        statusTransferRequest = merchantReportResponse.getStatus();
        assertEquals(200,statusTransferRequest);
    }


}

package com.dtupay.client.Models;
//@author Magnus glasdam jakobsen
public class ScanTokenDTO {

    public String customerID;
    public String merchantID;

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

}

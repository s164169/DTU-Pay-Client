package com.dtupay.client.Models;
//@author Frederik Kirkegaard
public class ScanResultDTO {
    boolean scanSuccesful;

    public boolean isScanSuccesful() {
        return scanSuccesful;
    }

    public void setScanSuccesful(boolean scanSuccesful) {
        this.scanSuccesful = scanSuccesful;
    }
}

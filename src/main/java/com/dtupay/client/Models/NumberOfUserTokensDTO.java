package com.dtupay.client.Models;
//@author Sebastian Fisher
public class NumberOfUserTokensDTO {
    public int numberOfTokens;


    public int getNumberOfTokens() {
        return numberOfTokens;
    }

    public void setNumberOfTokens(int numberOfTokens) {
        this.numberOfTokens = numberOfTokens;
    }
}

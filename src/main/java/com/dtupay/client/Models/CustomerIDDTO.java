package com.dtupay.client.Models;

import java.util.UUID;
//@author Sebastian Fisher
public class CustomerIDDTO {

    public UUID customerID;
    public String customerAccountID;


    public UUID getCustomerID() {
        return customerID;
    }

    public void setCustomerID(UUID merchantID) {
        this.customerID = merchantID;
    }

    public String getCutomerAccountID() {
        return customerAccountID;
    }
}

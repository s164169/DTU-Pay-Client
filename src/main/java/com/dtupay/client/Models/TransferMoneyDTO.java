package com.dtupay.client.Models;

import java.math.BigDecimal;
//@author William ben embarek

    public class TransferMoneyDTO {
        public String tokenID;
        public int tokenStatus;
        public String toAccount;
        public String fromAccount;
        public BigDecimal amount;
        public String description;

        public TransferMoneyDTO() {
        }

        public String getTokenID() {
            return this.tokenID;
        }

        public void setTokenID(String tokenID) {
            this.tokenID = tokenID;
        }

        public int getTokenStatus() {
            return this.tokenStatus;
        }

        public void setTokenStatus(int tokenStatus) {
            this.tokenStatus = tokenStatus;
        }

        public String getToAccount() {
            return this.toAccount;
        }

        public void setToAccount(String toAccount) {
            this.toAccount = toAccount;
        }

        public String getFromAccount() {
            return this.fromAccount;
        }

        public void setFromAccount(String fromAccount) {
            this.fromAccount = fromAccount;
        }

        public BigDecimal getAmount() {
            return this.amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public String getDescription() {
            return this.description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }


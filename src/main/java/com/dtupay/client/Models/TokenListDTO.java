package com.dtupay.client.Models;

import java.util.List;
//@author Sebastian Fisher

public class TokenListDTO {

    public TokenListDTO(){}

    List<TokenDTO> tokens;

    public List<TokenDTO> getTokens() {
        return tokens;
    }

    public void setTokens(List<TokenDTO> tokens) {
        this.tokens = tokens;
    }
}

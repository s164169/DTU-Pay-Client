package com.dtupay.client.Models;

import java.util.UUID;
//@author Mathias Bo Jensen
public class MerchantIDDTO {
    public UUID merchantID;
    public String merchantAccountID;

    public UUID getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(UUID merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantAccountID() {
        return merchantAccountID;
    }
}

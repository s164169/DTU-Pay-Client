package com.dtupay.client.Models;

import java.util.UUID;
//@author Mathias Bo Jensen
public class TokenDTO {
    public enum TokenStatus{UNUSED, ACCEPTED, REJECTED};
    private String tokenID;
    private TokenStatus status;
    private String customerID;



    public TokenDTO(){}

    public String getTokenID() {
        return tokenID;
    }

    public void setStatus(TokenStatus status){
        this.status = status;
    }

    public TokenStatus getStatus() {
        return status;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }
}

package com.dtupay.client;


import com.dtupay.client.Converters.ConvertAccount;
import com.dtupay.client.Converters.ConvertAccountInfo;
import com.dtupay.client.Converters.ConvertUser;
import com.dtupay.client.Models.DTUPayAccount;
import com.dtupay.client.Models.DTUPayAccountInfo;
import com.dtupay.client.Models.DTUPayUser;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;

import java.math.BigDecimal;
//@author Frederik Kirkegaard
public class SoapBankAdapter implements IBank {
    BankService bank = new BankServiceService().getBankServicePort();
    @Override
    public String createAccountWithBalance(DTUPayUser dtuPayUser, BigDecimal initialBalance) {
        try {
            return bank.createAccountWithBalance(ConvertUser.covertUser(dtuPayUser), initialBalance);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public DTUPayAccount getAccount(String accountNumber) {
        try {
            return ConvertAccount.convertAccount(bank.getAccount(accountNumber));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public DTUPayAccount getAccountByCPRNumber(String cpr) {
        try {
            return ConvertAccount.convertAccount(bank.getAccountByCprNumber(cpr));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public DTUPayAccountInfo[] getAccounts() {
        return (DTUPayAccountInfo[]) bank.getAccounts().stream().map(ConvertAccountInfo::convertAccountInfo).toArray();
    }

    @Override
    public void retireAccount(String accountNumber) {
        try {
            bank.retireAccount(accountNumber);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void transferMoneyFromTo(String accountFrom, String accountTo, BigDecimal amount, String description) {
        try {
            bank.transferMoneyFromTo(accountFrom,accountTo,amount,description);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }
}

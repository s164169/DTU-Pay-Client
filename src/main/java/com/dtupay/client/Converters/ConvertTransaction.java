package com.dtupay.client.Converters;

import com.dtupay.client.Models.DTUPayTransaction;
import dtu.ws.fastmoney.Transaction;

//@author Magnus glasdam jakobsen
public class ConvertTransaction {
    public static DTUPayTransaction convertTransaction(Transaction transaction) {
        DTUPayTransaction dtuPayTransaction = new DTUPayTransaction();
        dtuPayTransaction.setAmount(transaction.getAmount());
        dtuPayTransaction.setBalance(transaction.getBalance());
        dtuPayTransaction.setCreditor(transaction.getCreditor());
        dtuPayTransaction.setDebtor(transaction.getDebtor());
        dtuPayTransaction.setDescription(transaction.getDescription());
        dtuPayTransaction.setTime(transaction.getTime());
        return dtuPayTransaction;
    }
}

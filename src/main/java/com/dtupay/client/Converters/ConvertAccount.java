package com.dtupay.client.Converters;

import com.dtupay.client.Models.DTUPayAccount;
import dtu.ws.fastmoney.Account;

import java.util.stream.Collectors;

//@author Mathias Boss Jørgensen
public class ConvertAccount {

    public static DTUPayAccount convertAccount(Account account) {
        DTUPayAccount dtuPayAccount = new DTUPayAccount();
        dtuPayAccount.setBalance(account.getBalance());
        dtuPayAccount.setId(account.getId());
        dtuPayAccount.setUser(ConvertUser.revertUser(account.getUser()));
        dtuPayAccount.setTransactions(account.getTransactions().stream().map(ConvertTransaction::convertTransaction).collect(Collectors.toList()));
        return dtuPayAccount;
    }
}

package com.dtupay.client.Converters;

import com.dtupay.client.Models.DTUPayUser;
import dtu.ws.fastmoney.User;

import java.util.UUID;
//@author Mathias Bo Jensen
public class ConvertUser {

    public static User covertUser(DTUPayUser dtuPayUser) {
        User user = new User();
        user.setCprNumber(dtuPayUser.getCprNumber());
        user.setFirstName(dtuPayUser.getFirstName());
        user.setLastName(dtuPayUser.getLastName());
        return user;
    }

    public static DTUPayUser revertUser(User user) {
        DTUPayUser dtuPayUser = new DTUPayUser(user.getCprNumber(),user.getFirstName(),user.getLastName(), UUID.randomUUID(), "AccountID");
        return dtuPayUser;
    }
}

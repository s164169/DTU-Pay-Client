package com.dtupay.client.Converters;

import com.dtupay.client.Models.DTUPayAccountInfo;
import dtu.ws.fastmoney.AccountInfo;

//@author Frederik Kirkegaard
public class ConvertAccountInfo {
    public static DTUPayAccountInfo convertAccountInfo(AccountInfo accountInfo) {
        DTUPayAccountInfo dtuPayAccountInfo = new DTUPayAccountInfo();
        dtuPayAccountInfo.setAccountId(accountInfo.getAccountId());
        dtuPayAccountInfo.setUser(ConvertUser.revertUser(accountInfo.getUser()));
        return dtuPayAccountInfo;
    }
}

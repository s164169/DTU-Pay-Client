package com.dtupay.client;


import com.dtupay.client.Models.DTUPayAccount;
import com.dtupay.client.Models.DTUPayAccountInfo;
import com.dtupay.client.Models.DTUPayUser;

import java.math.BigDecimal;
//@author Magnus glasdam jakobsen
public interface IBank {

    public String createAccountWithBalance(DTUPayUser dtuPayUser, BigDecimal initialBalance) throws Exception;

    public DTUPayAccount getAccount(String accountNumber) throws Exception;

    public DTUPayAccount getAccountByCPRNumber(String cpr) throws Exception;

    public DTUPayAccountInfo[] getAccounts() throws Exception;

    public void retireAccount(String accountNumber) throws Exception;

    public void transferMoneyFromTo(String accountFrom, String accountTo, BigDecimal amount, String description) throws Exception;

}
